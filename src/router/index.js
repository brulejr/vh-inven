/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import Vue from 'vue'
import Router from 'vue-router'

import hoodie from '@/hoodie'

import LoginPage from '@/pages/public/LoginPage'
import PublicApp from '@/pages/public/PublicApp'
import SignupPage from '@/pages/public/SignupPage'
import WelcomePage from '@/pages/public/WelcomePage'

import HomePage from '@/pages/private/HomePage'
import MainApp from '@/pages/private/MainApp'

Vue.use(Router)

const anonymousRoutes = {
  path: '/',
  component: PublicApp,
  children: [
    {
      path: '',
      component: WelcomePage
    },
    {
      path: 'login',
      component: LoginPage
    },
    {
      path: 'signup',
      component: SignupPage
    }
  ]
}

const appRoutes = {
  path: '/app',
  component: MainApp,
  meta: { requiresAuth: true },
  children: [
    {
      path: '',
      component: HomePage
    }
  ]
}

const router = new Router({
  mode: 'history',
  base: '/',
  routes: [
    anonymousRoutes,
    appRoutes
  ]
})

router.beforeEach((to, from, next) => {
  hoodie.account.get({local: true}).then(user => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (!user.session) {
        return {path: '/login', query: {redirect: to.fullPath}}
      }
    }
    return undefined
  }).then(path => {
    console.log('path', path)
    next(path)
  })
})

export default router
